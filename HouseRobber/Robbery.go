package HouseRobber

import "errors"

func Robbery(moneys []int) (int, error) {
	var last, curr int
	for _, value := range moneys {

		isNegative := checkNegative(value)

		if isNegative {
			return 0, errors.New("Array contains negative value")
		}
		temp := curr
		curr = findMaximum(last+value, curr)
		last = temp
	}
	return curr, nil
}

func findMaximum(a, b int) int {

	if a > b {
		return a
	}
	return b
}

func checkNegative(num int) bool {

	if num < 0 {
		return true
	}
	return false
}
