package HouseRobber

import (
	"errors"
	"fmt"
)

//HouseRobberMain is starting func of house robber program
func HouseRobberMain() {

	inputNum := []int{2, 3, 2}

	fmt.Println("Houses with money:", inputNum)
	maxRob, err := RobMoney(inputNum)
	if err != nil {
		fmt.Println("Error:", err)
	} else {
		fmt.Println("Maximum Money Rob:", maxRob)

	}
}

func RobMoney(moneys []int) (int, error) {
	if len(moneys) == 0 {
		return 0, errors.New("Array is Empty")
	}
	if len(moneys) == 1 {
		return moneys[0], nil
	}

	//include 1st home to Robbery
	rob1Money, err := Robbery(moneys[0 : len(moneys)-1])
	if err != nil {
		return 0, err
	}

	//include last home to Robbery
	rob2Money, err := Robbery(moneys[1:])
	if err != nil {
		return 0, err
	}

	if rob1Money > rob2Money {
		return rob1Money, nil
	}
	return rob2Money, nil
}
