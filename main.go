package main

import (
	"fmt"

	"gitlab.com/Harikesh00/harikeshprajapati-golangcodingexercise-golangsoftwareengineer/BinarySearchTree"
	"gitlab.com/Harikesh00/harikeshprajapati-golangcodingexercise-golangsoftwareengineer/HouseRobber"
)

func main() {
	fmt.Println("************** Assignment 1 (Binary Search Tree) *****************\n")
	BinarySearchTree.BinarySearchTreeMain()

	fmt.Println("\n\n************** Assignment 2 (House Robber) *****************\n")
	HouseRobber.HouseRobberMain()
}
