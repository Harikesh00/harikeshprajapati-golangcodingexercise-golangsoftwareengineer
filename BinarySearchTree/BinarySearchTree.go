package BinarySearchTree

import "fmt"

//Node of tree to represet binary tree
type Node struct {
	data  int
	left  *Node
	right *Node
}

type Tree struct {
	root *Node
}

//BinarySearchTreeMain is starting func of binary tree program
func BinarySearchTreeMain() {

	t := &Tree{}

	fmt.Println("Input data:")

	//Note:- If chars inserted, they will converted to their ASCII code eg. t.Insert('a')

	//first node will be the root
	t.Insert(80)
	t.Insert(40)
	t.Insert(100)
	t.Insert(20)
	t.Insert(60)
	t.Insert(10)
	t.Insert(30)
	t.Insert(50)
	t.Insert(70)
	t.Insert(90)

	err := t.InOrder()
	if err != nil {
		fmt.Println(err)
	}

	t.PreOrder()
	if err != nil {
		fmt.Println(err)
	}

	t.PostOrder()
	if err != nil {
		fmt.Println(err)
	}
}

//Insert item to tree
func (t *Tree) Insert(value int) {

	fmt.Print(" ", value, " ")
	node := &Node{value, nil, nil}

	// if tree empty make 1st node root
	if t.root == nil {
		t.root = node
	} else {
		InsertNodeIntoTree(t.root, node)
	}

}

//InsertNodeIntoTree insert item to correct position in tree
func InsertNodeIntoTree(node, newNode *Node) {

	//insert node to left
	if newNode.data < node.data {
		if node.left == nil {
			node.left = newNode
		} else {
			InsertNodeIntoTree(node.left, newNode)
		}
	} else { //insert node to right
		if node.right == nil {
			node.right = newNode
		} else {
			InsertNodeIntoTree(node.right, newNode)
		}
	}

}
