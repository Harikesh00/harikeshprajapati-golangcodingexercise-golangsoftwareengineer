package BinarySearchTree

import (
	"errors"
	"fmt"
)

func (t *Tree) InOrder() error {
	fmt.Println("\n\n------------------------ In order ------------------------")
	if t.root == nil {
		return errors.New("Tree is empty")
	} else {
		PrintInOrder(t.root)
	}
	return nil
}

func PrintInOrder(n *Node) {

	if n != nil {
		PrintInOrder(n.left)
		fmt.Print(" -> ", n.data)
		PrintInOrder(n.right)
	}

}
