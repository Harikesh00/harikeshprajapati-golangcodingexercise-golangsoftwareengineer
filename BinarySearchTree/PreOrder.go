package BinarySearchTree

import (
	"errors"
	"fmt"
)

func (t *Tree) PreOrder() error {
	fmt.Println("\n\n------------------------ Pre order ------------------------")
	if t.root == nil {
		return errors.New("Tree is empty")
	} else {
		PrintPreOrder(t.root)
	}
	return nil
}

func PrintPreOrder(n *Node) {
	if n != nil {
		fmt.Print(" -> ", n.data)
		PrintPreOrder(n.left)
		PrintPreOrder(n.right)
	}

}
