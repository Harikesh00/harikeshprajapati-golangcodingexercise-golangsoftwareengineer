package BinarySearchTree

import (
	"errors"
	"fmt"
)

func (t *Tree) PostOrder() error {
	fmt.Println("\n\n------------------------ Post order ------------------------")
	if t.root == nil {
		return errors.New("Tree is empty")
	} else {
		PrintPostOrder(t.root)
	}
	return nil
}

func PrintPostOrder(n *Node) {

	if n != nil {
		PrintPostOrder(n.left)
		PrintPostOrder(n.right)
		fmt.Print(" -> ", n.data)
	}
}
